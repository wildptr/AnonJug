var helper = {
    convertCRLFtoBR : function( str ) {
        return str.replace("\r\n","<br/>");
    },
    parseJson : function( str )
    {
        if( str[0] !== "[" && str[0] !== "{" )
            return null;
        return eval("(" + str + ")");
    },
    parseHttpReq : function( obj ) //将OBJ转换成HTTP发送格式
    {
        var str = ""
        for(var key in obj )
        {
            str += key.toString() + "=" + obj[key] ;
            str += "&";
        }

        str = str.substring(0,str.length-1); //去除最后一个&

        console.log( str );
        return str;
    },
    parseFormdata : function( obj )
    {
        var formData = new FormData;
        for(var key in obj)
        {
            formData.append( key, obj[key] );
        }
    },
	sendHttpReq : function(type, url, data, func, obj)
	{
		var xhr = new XMLHttpRequest();
		xhr.onreadystatechange = function () {
			if (xhr.readyState === 4)
			{
				var responseStr = xhr.responseText;
				// console.log( responseStr );
				func.call( obj, responseStr );
			}
		};
        var strData = this.parseHttpReq( data );
		if( type === "POST" ) //POST的话，需要加上HTTP HEAD
		{
		    xhr.open(type, url, true);
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            xhr.send( strData );
		}
        else if ( type === "GET" )
        {
            url += "?" + strData;
            xhr.open(type,url,true);
            xhr.send();
        }
	},
	sendHttpGet: function ( msg ) //url, data, func, obj)
	{
		this.sendHttpReq("GET",msg.url,( msg.data ),msg.func,msg.obj);
	},
	sendHttpPost : function( msg )// url, data, func, obj )
	{
		this.sendHttpReq("POST",msg.url,( msg.data ),msg.func,msg.obj);
	}
};