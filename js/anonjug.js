var anonjug;
(function() {
	anonjug = {
		jug_list : null,
		write_jug : null,
		curJugList : [], //当前帖子对象。
		sess : null,
		onError : function( res )
		{
			if( !res ) return false;
			if( res.ret === 1 )
			{
				alert( res.error );
				return false;
			}
			return true;
		},
		onload : function() //body载入后。
		{
			//check sess
			this.checkSess();
		},
		checkSess : function()
		{
			helper.sendHttpPost({
				url : "server/p.do",
				data : {
					do : "login"
				},
				func : function( responseStr ) {
					console.log( responseStr );
					var res = helper.parseJson( responseStr );
					this.onError( res );
					document.getElementById("anonName").innerText = res.sess.curName; 
					this.sess = res.sess; 
				},
				obj : this
			});
		},
		showFavorite : function( ) //显示收藏
		{
			helper.sendHttpGet({
				url : "server/g.do",
				data : {
					do : "jug_get",
					id : JSON.stringify( this.sess.favorite ),
					letterShow : 3, //-1表示获取全部，0无，1-N 数量。
					tpl : "jug_list",
				},
				func : function( responseStr ) {
					var res = helper.parseJson( responseStr );
					this.onError( res );
					this.jug_list.innerHTML = res.tpl;
					this.curJugList = res.jugs;
				},
				obj : this
			});
		},
		onLoadContent : function( ele ) //CONTENT载入后
		{
			console.log( "content onload" );
			console.log( ele );
			this.jug_list = ele.firstElementChild;
			this.write_jug = this.jug_list.nextElementSibling;
			
			helper.sendHttpGet({
				url : "server/g.do",
				data : {
					do : "jug_get",
					page : 1,
					letterShow : 3, //-1表示获取全部，0无，1-N 数量。
					tpl : "jug_list",
				},
				func : function( responseStr ) {
					var res = helper.parseJson( responseStr );
					this.onError( res );
					this.jug_list.innerHTML = res.tpl;
					this.curJugList = res.jugs;
				},
				obj : this
			});

			helper.sendHttpGet({
				url : "server/g.do",
				data : {
					do : "tpl",
					tpl : "write_jug",
				},
				func : function( responseStr ) {
					var res = helper.parseJson( responseStr );
					this.onError( res );
					this.write_jug.innerHTML = res.tpl;
				},
				obj : this
			});
		},

		onPutJug : function() //发帖
		{
			var writer = document.getElementById( "write_jug" );
			var tag   = writer.children[0].children[0];
			var title = writer.children[0].children[1];
			var text  = writer.children[1];

			helper.sendHttpPost({
				url : "server/p.do",
				data : {
					do : "jug_new",
					tpl : "jug_list",
					tag : encodeURIComponent( tag.value ),
					title : encodeURIComponent( title.value ),
					text : encodeURIComponent( text.value )
				},
				func : function( responseStr ) {
					var res = helper.parseJson( responseStr );
					this.onError( res );
					this.jug_list.innerHTML = res.tpl + this.jug_list.innerHTML;
					this.curJugList = concat( this.curJugList, res.jugs );//.push.apply( this.curJugList, res.jugs );
				},
				obj : this
			});

			console.log( "onPutJug" );
		},
		onPutLetter : function( ele ) //回复帖子
		{
			var jugEle = ele.parentNode.parentNode.parentNode.parentElement;
			var letterList = ele.parentNode.parentElement;
			var text   = ele.parentElement.children[0];//text.value;
			var id = parseInt( jugEle.id );

			helper.sendHttpPost({
				url : "server/p.do",
				data : {
					do : "letter_new",
					tpl : "letter",
					jugId : id,
					text : encodeURIComponent( text.value )
				},
				func : function( responseStr ) {
					var res = helper.parseJson( responseStr );
					this.onError( res );
					letterList.innerHTML = letterList.innerHTML.replace('<div id="write_letter">', res.tpl + '<div id="write_letter">' );
					this.curJugList[id].letters.push( res.letter );
				},
				obj : this
			});
		},
		onFavorite : function( btn, id ) //点击了收藏JUG
		{
			helper.sendHttpPost({
				url : "server/p.do",
				data : {
					do : "favorite_jug",
					type : 1,
					jugId : id
				},
				func : function( responseStr )
				{
					var res = helper.parseJson( responseStr );
					this.onError( res );
					btn.value = "取消收藏";
					btn.onclick = this.onUnFavoriteJug;
				},
				obj : this
			});
		},
		onUnFavorite : function( btn, id ) //点击 不收藏JUG
		{
			helper.sendHttpPost({
				url : "server/p.do",
				data : {
					do : "favorite_jug",
					type : 0,
					jugId : id
				},
				func : function( responseStr )
				{
					var res = helper.parseJson( responseStr );
					this.onError( res );
					btn.value = "收藏";
					btn.onclick = this.onFavoriteJug;
				},
				obj : this
			});
		},
		onAutoRefresh : function() //自动刷新
		{
			helper.sendHttpGet({
				url : "server/g.do",
				data :{
					do : "jug_get",
					page : 1,
					letterShow : 3,
					tpl : "jug_list",
				},
				func : function( responseStr ) {
					var res = helper.parseJson( responseStr );
					this.onError( res );
					this.jug_list.innerHTML = res.tpl; //debug
					this.curJugList = res.jugs;
				},
				obj : this
			});
		}
	};
	// anonjug._onAutoRefreshIntervalId = setInterval( anonjug.onAutoRefresh.bind(anonjug), 10000 ); //debug 10秒
})();