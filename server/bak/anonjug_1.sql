
-- -----------------------------
-- Table `jugs` structure;
-- -----------------------------
DROP TABLE IF EXISTS `jugs`;
CREATE TABLE `jugs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author` char(8) NOT NULL DEFAULT '',
  `tag` char(8) NOT NULL DEFAULT '' COMMENT '文章标签',
  `title` char(32) NOT NULL,
  `text` text,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`tag`,`author`,`title`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Table `letter` structure;
-- -----------------------------
DROP TABLE IF EXISTS `letter`;
CREATE TABLE `letter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jug_id` int(11) NOT NULL,
  `author` char(8) NOT NULL,
  `text` varchar(255) NOT NULL DEFAULT '',
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`,`jug_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- -----------------------------
-- Table `favorite` structure;
-- -----------------------------
DROP TABLE IF EXISTS `favorite`;
CREATE TABLE `favorite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cookie_id` char(32) NOT NULL,
  `jug_id` int(11) NOT NULL COMMENT '访问时间',
  `prev_visit_time` int(10) NOT NULL DEFAULT '0' COMMENT '上次访问时间',
  PRIMARY KEY (`cookie_id`,`prev_visit_time`,`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- -----------------------------
-- Table `jugs` data;
-- -----------------------------
-- Begin 1 / 1 ------------------------
INSERT INTO `jugs` VALUES (1,'dogking','test','系统测试','这里是系统测试','2016-11-24 19:38:53');
INSERT INTO `jugs` VALUES (2,'catking','test','系统测试2','西斯isi一已if一i','2016-11-25 19:39:10');
INSERT INTO `jugs` VALUES (3,'star','css','CSS如何注释','我在存储很长的汉字时遇到这个错误：\r\nERROR 1118 (42000): Row size too large. The maximum row size for the used table type, not counting BLOBs, is 65535. You have to change some columns to TEXT or BLOBs。\r\n在网上搜时，发现都不能解决。我又换了下问题。“mysql里怎么存储很多汉字”\r\n搜到网友的如下回答：先前换成text的貌似也不行哎。后面换成bigtext的就可以了。先前的text我用的是大写不知道为什么不能保存。后面换成这个bigtext就能保存1000多个汉字了。特写下来给网友参考下。\r\n参考网友的说法如下\r\nhttp://zhidao.baidu.com/question/51279502.html\r\n我用MYSQL存储论坛和博客文章的数据库，BIGTEXT类型，从来没有发现满过，尽管有人贴长篇小说。\r\n这里就是把默认的varchar存储类型换成bigtext类型就行了。可以存储好多汉字。','2016-11-26 15:16:49');
-- End 1 / 1 --------------------------

-- -----------------------------
-- Table `letter` data;
-- -----------------------------
-- Begin 1 / 1 ------------------------
INSERT INTO `letter` VALUES (1,2,'wo','yes','0000-00-00 00:00:00');
INSERT INTO `letter` VALUES (2,2,'wo','yesno','2016-11-25 19:38:02');
INSERT INTO `letter` VALUES (3,1,'ni','noyes','2016-11-23 19:38:05');
INSERT INTO `letter` VALUES (4,1,'fdsafdsa','erwrrrere','2016-11-28 19:38:08');
-- End 1 / 1 --------------------------

-- -----------------------------
-- Table `favorite` data;
-- -----------------------------
