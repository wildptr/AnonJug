<li id="{{letter.id}}">
    -<span id="letter_text">{{ htmlencode(letter.text) }}</span>
    <span class="fontS fontMinor">by 
        <span id="letter_author">{{ htmlencode(letter.author) }}</span> 
        <span id="letter_time">{{ timeformat(letter.time,"<yr>年<mo>月<dy>日 <h>:<m>:<s>") }}</span>
    </span>
</li>