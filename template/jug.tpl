<div class="jug_node" id="{{jug.id}}"> <span class="fontL">[{{ htmlencode(jug.tag) }}]</span><span class="fontL fontTitle">{{ htmlencode(jug.title) }}</span> <span class="fontS fontMinor"><span class="author">{{ htmlencode(jug.author) }}</span> <span>{{ timeformat(letter.time,"<yr>年<mo>月<dy>日 <h>:<m>:<s>") }}</span></span>
<{if=jug.favorite}>
    <input type="button" value="取消收藏" onclick="anonjug.onUnFavorite(this,{{jug.id}})" />
<{else}>
    <input type="button" value="收藏" onclick="anonjug.onFavorite(this,{{jug.id}})" />
<{/if}>

    <div class="jug_detail">
        -{{ htmlencode(jug.text) }}
        <div class="jug_letters">
            <{loop=jug.letters letter }>
                <{load=letter.tpl}>
            <{/loop}>
            <div id="write_letter" >
            <{load=write_letter.tpl}>
            </div>
        </div>
    </div>
</div>